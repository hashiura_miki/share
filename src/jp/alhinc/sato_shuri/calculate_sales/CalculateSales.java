package jp.alhinc.sato_shuri.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {



		BufferedReader br = null;
		//System.out.println(args[0]);


		try{

			//Fileクラスのオブジェクトを生成する
			File dir = new File(args[0]);

			//listFilesメソッドを使用して一覧を取得する
			File[] list = dir.listFiles();
			for (int i = 0; i < list.length; i ++) {
				String allFile =list[i].getName();
				System.out.println(allFile);

				/*String[] saleinfo = allFile.split(".");


				BufferedReader sr = new BufferedReader(new FileReader(allFile));

				String sale = sr.readLine();

				String[] saleinfo = sale.split(".",2);
				System.out.println(saleinfo);*/


				// [0-9]{8} 8桁の数値

			};



			File file = new File(args[0],"branch.lst");

			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String str = br.readLine();




			while(str != null){
				String[] branchInfo = str.split(",", 2);
				//System.out.println(str);

				String code = branchInfo[0];
				//System.out.println(code);

				if (!isCode(code)) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				String branch = branchInfo[1];
				//System.out.println(branch);

				if (!isBranch(branch)) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				Map<String, String> branchMap = new HashMap<>();

				branchMap.put(code, branch);

				for (Map.Entry<String, String> entry : branchMap.entrySet()) {
					//System.out.println(entry.getKey() + "," + entry.getValue());
				}



				str = br.readLine();
			}


		}catch(FileNotFoundException e){
			System.out.println(e);
		}catch(IOException e){
			System.out.println(e);
		}finally {
			if (br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("closeできませんでした");
				}
			}

		}



	}



	public static boolean isBranch(String branch) {
		// TODO: 要素数で判断
		if (branch.contains(",") || branch.contains("\n")) {
			return false;
		}else {
			return true;
		}
	}

	public static boolean isCode(String code) {
		try {
			Integer.parseInt(code);
			// TODO: 正規表現に修正
			if (code.length()==3) {
				return true;
			} else {
				return false;
			}

		} catch (NumberFormatException e) {
			return false;
		}
	}
}